# gitlab-serverless-framework

An example project of deploying a AWS Lambda function + API Gateway using Serverless Framework and gitlab-ci

### Setup

```
npm install
```

### Deploy

```
serverless deploy --stage staging
```